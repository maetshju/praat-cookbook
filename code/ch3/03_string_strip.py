from praatcommands import PraatCommands

s = '  hello '
pc = PraatCommands(exe='/home/matt/praat')

commands = [
    's$ = "{}"'.format(s.strip()),
    'writeInfoLine: s$'
]

pc.cmds = commands
pc.run_praat_commands(print_output=True)
