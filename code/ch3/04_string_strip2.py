from praatcommands import PraatCommands

pc = PraatCommands(exe='/home/matt/praat')

commands = [
    's$ = "  this is a test "',
    'writeInfoLine: s$'
]

pc.cmds = commands
s = pc.run_praat_commands()[0]
print(s.strip())
