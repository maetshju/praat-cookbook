from praatcommands import PraatCommands
import os

pc = PraatCommands(exe='/home/matt/praat')
dir = '/home/matt/audio/'

commands = []

for fName in os.listdir(dir):

    fullpath = os.path.join(dir, fName)
    base = os.path.splitext(fName)[0]
    commands += [
        'Read from file: "{}"'.format(fullpath),
        'select Sound {}'.format(base),
        'dur = Get total duration',
        'appendInfoLine: "{}{}{}"'.format(base, '\t', "'dur'"),
        'Remove'
    ]

pc.cmds = commands
pc.run_praat_commands(print_output=True)
