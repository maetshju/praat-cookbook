# Collect arguments to the script
form Strip whitespace from TextGrids
    comment "What directory contains the TextGrids?"
    text dir /home/matt/grids
    comment "Which tier do you want to check?"
    integer tier 1
    comment "What subdirectory should the modified TextGrids be written to?"
    text outdir stripped_grids
endform

# Get names of all TextGrid files
Create Strings as file list: "list", "'dir$'/*.TextGrid"
nFiles = Get number of strings

# Create subdirectory to hold new files
fulloutdir$ = dir$ + "/" + outdir$
createDirectory: fulloutdir$

# Iterate through each TextGrid
for iFile from 1 to nFiles

    select Strings list
    gridFile$ = Get string: iFile
    gridPath$ = dir$ + "/" + gridFile$

    # Get base name of TextGrid to allow selecting it in Praat
    grid$ = replace$(gridFile$, ".TextGrid", "", 0)

    Read from file: gridPath$
    select TextGrid 'grid$'


    # Iterate throug heach interval and strip its label
    nIntervals = Get number of intervals: tier

    for iInterval from 1 to nIntervals
        lab$ = Get label of interval: tier, iInterval
        @strip: lab$
        lab$ = strip.stripped$
        Set interval text: tier, iInterval, lab$
    endfor

    # Save and clean up
    Save as text file: fulloutdir$ + "/" + gridFile$
    select TextGrid 'grid$'
    Remove
endfor

select Strings list
Remove

procedure strip: .s$

  	.len = length(.s$)
  	.lindex = index_regex(.s$, "[^ \n\t\r]")
  	.beginning$ = right$(.s$, .len - .lindex + 1)
  	.rindex = index_regex(.beginning$, "[ \n\t\r]*$")
  	.stripped$ = left$(.beginning$, .rindex-1)
endproc
