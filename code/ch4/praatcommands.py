import tempfile
import subprocess
import time

class PraatCommands:

    def __init__(self, cmds=[], exe='praat', args=[], gui=False, sendpraat=None, newinstance=True):

        self.cmds = cmds
        self.exe = exe
        self.args = args
        self.gui = gui
        self.sendpraat = sendpraat
        self.newinstance = newinstance

    def run_praat_commands(self, print_output=False):

        tname = None

        with tempfile.NamedTemporaryFile(mode='w', suffix='.praat', delete=False) as t:

            tname = t.name

            for c in self.cmds:

                t.write('{}\n'.format(c))

        if self.gui:

            if self.newinstance:
                subprocess.Popen(self.exe)
                time.sleep(2)

            call_command = [self.sendpraat, 'praat',  'runScript: "{}"'.format(t.name)]
            output = subprocess.call(call_command)
            return

        else:
            call_command = [self.exe, '--run', t.name] + self.args
            output = subprocess.check_output(call_command, universal_newlines=True)
            if print_output:
                print(output, end='')
            return output.rstrip().split('\n')

def main():

    praat = PraatCommands(exe='/home/matt/praat')
    praat.cmds.append('writeInfoLine: "test"')
    praat.cmds.append('appendInfoLine: "test2"')
    print(praat.run_praat_commands())

if __name__ == '__main__':
    main()
