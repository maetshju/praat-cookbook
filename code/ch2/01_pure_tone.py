from praatcommands import PraatCommands
toneargs = '"tone", 1, 0, 1, 44100, 440, 0.2, 0.01, 0.01'
commands = [
    'Create Sound as pure tone: {}'.format(toneargs),
    'Play'
]
pc = PraatCommands(exe='/home/matt/praat', cmds=commands)
pc.run_praat_commands()
