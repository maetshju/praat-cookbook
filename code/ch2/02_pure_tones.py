from praatcommands import PraatCommands

commands = list()

for i in range(1000, 2000, 100):

    toneargs = '"tone", 1, 0, 0.2, 44100, {}, 0.2, 0.01, 0.01'.format(i)
    commands += [
        'Create Sound as pure tone: {}'.format(toneargs),
        'Play',
        'Remove'
    ]

pc = PraatCommands(exe='/home/matt/praat', cmds=commands)
pc.run_praat_commands()
