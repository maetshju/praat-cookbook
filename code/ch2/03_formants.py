from praatcommands import PraatCommands
import os

stimdir = '/home/matt/stimuli'
pc = PraatCommands(exe='/home/matt/praat')
formants_by_item = dict()

# iterate through all files ending in wav
for fname in [x for x in os.listdir(stimdir) if x.endswith('wav')]:

    item = os.path.splitext(fname)[0]
    fullpath = os.path.join(stimdir, fname)

    commands = [
      'Read from file: "{}"'.format(fullpath),
      'To Formant (burg): 0, 5, 5500, 0.025, 50',
      'f1 = Get mean: 1, 0, 0, "Hertz"',
      'f2 = Get mean: 2, 0, 0, "Hertz"',
      'writeInfoLine: f1',
      'writeInfoLine: f2',
    ]

    pc.cmds = commands
    formants = pc.run_praat_commands()

    # convert the output to floats
    formants = list(map(float, formants))

    formants_by_item[item] = formants
