from praatcommands import PraatCommands
import os

def get_formants_1_2(dir, exe='praat'):

    pc = PraatCommands(exe)
    for fname in [x for x in os.listdir(dir) if x.endswith('wav')]:

        item = os.path.splitext(fname)[0]
        fullpath = os.path.join(dir, fname)

        commands = [
          'Read from file: "{}"'.format(fullpath),
          'To Formant (burg): 0, 5, 5500, 0.025, 50',
          'f1 = Get mean: 1, 0, 0, "Hertz"',
          'f2 = Get mean: 2, 0, 0, "Hertz"',
          'writeInfoLine: f1',
          'writeInfoLine: f2',
        ]

        pc.cmds = commands
        formants = pc.run_praat_commands()

        # convert the output to floats
        formants = list(map(float, formants))

    return formants
