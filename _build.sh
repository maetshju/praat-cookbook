Rscript -e "install.packages('bookdown')"
Rscript -e "bookdown::render_book('praat_cookbook.Rmd','bookdown::gitbook', output_dir='public')"
cp public/preface.html public/index.html
